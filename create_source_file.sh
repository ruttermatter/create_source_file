#!/bin/bash

#: Title       : create_source_file
#: Date        : 2018-01-03
#: Author      : Matthew Rutter <shellscripts@matt-rutter.com>
#: Version     : 1.0
#: Description : This script creates and writes templates to newly created project files based on the type of project being created. The arguments that are passed into the script are the project location/name (full directory name) in addition to a string denoting the project type.
#: Options     : None

echo Hello World
